package com.yulang.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.server.pojo.Chapter;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-04-23
 */
@Mapper
public interface ChapterMapper extends BaseMapper<Chapter> {

}
