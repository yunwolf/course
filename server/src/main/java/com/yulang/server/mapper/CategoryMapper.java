package com.yulang.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.server.pojo.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 分类 Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

}
