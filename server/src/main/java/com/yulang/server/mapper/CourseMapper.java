package com.yulang.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.server.pojo.Course;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Mapper
public interface CourseMapper extends BaseMapper<Course> {

}
