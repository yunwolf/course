package com.yulang.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.server.pojo.File;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 文件 Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Mapper
public interface FileMapper extends BaseMapper<File> {

}
