package com.yulang.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.server.pojo.Teacher;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Mapper
public interface TeacherMapper extends BaseMapper<Teacher> {

}
