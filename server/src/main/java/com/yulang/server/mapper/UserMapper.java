package com.yulang.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.server.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-05-18
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
