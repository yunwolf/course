package com.yulang.server.mapper;

import com.yulang.server.pojo.Test;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TestMapper {

    public List<Test> list();

}
