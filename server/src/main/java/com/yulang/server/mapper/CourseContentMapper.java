package com.yulang.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulang.server.pojo.CourseContent;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 课程内容 Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Mapper
public interface CourseContentMapper extends BaseMapper<CourseContent> {

}
