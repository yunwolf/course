package com.yulang.server.dto;

import lombok.Data;

@Data
public class IdLabel {

    private String id;

    private String label;

}
