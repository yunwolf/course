package com.yulang.server.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Administrator
 * @since 2020-04-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("chapter")
public class ChapterDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    @TableField(COURSE_ID)
    private String courseId;

    @TableField(NAME)
    private String name;


    public static final String ID = "`id`";

    public static final String COURSE_ID = "`course_id`";

    public static final String NAME = "`name`";


    public static final String ID_COMMENT = "";

    public static final String COURSE_ID_COMMENT = "";

    public static final String NAME_COMMENT = "";



    public static ChapterDto of(Object obj) {
        ChapterDto e = new ChapterDto();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<ChapterDto> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
