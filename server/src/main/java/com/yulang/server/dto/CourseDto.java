package com.yulang.server.dto;

import com.yulang.server.pojo.Course;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CourseDto extends Course {

    private List<String> categoryIds;

}
