package com.yulang.server.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

@Data
public class Pageable {

    private Integer page;

    private Integer size;


    public Page toPage(){
        Page pages = new Page();
        pages.setCurrent(this.page);
        pages.setSize(this.size);
        return pages;
    }

}
