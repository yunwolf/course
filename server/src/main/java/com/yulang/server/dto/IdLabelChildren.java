package com.yulang.server.dto;

import lombok.Data;

import java.util.List;

@Data
public class IdLabelChildren extends IdLabel {

    private List<IdLabel> children;

}
