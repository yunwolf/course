package com.yulang.server.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yulang.server.pojo.Category;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 分类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CategoryDto extends Category {

    private List<String> categoryIds;

}
