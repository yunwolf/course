package com.yulang.server.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-05-18
 */
public interface UserService extends IService<User> {

}
