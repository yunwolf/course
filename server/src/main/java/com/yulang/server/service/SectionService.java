package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.pojo.Section;

/**
 * <p>
 * 小节 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
public interface SectionService extends IService<Section> {

    void saveSection(Section section);
}
