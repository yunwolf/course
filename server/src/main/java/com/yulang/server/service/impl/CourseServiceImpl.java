package com.yulang.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulang.server.dto.CourseDto;
import com.yulang.server.mapper.CourseMapper;
import com.yulang.server.pojo.Course;
import com.yulang.server.pojo.CourseCategory;
import com.yulang.server.service.CourseCategoryService;
import com.yulang.server.service.CourseService;
import com.yulang.server.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

    @Autowired
    private CourseCategoryService courseCategoryService;

    @Autowired
    private CourseService service;

    @Transactional
    @Override
    public void saveCourse(CourseDto course) {
        course.setId(UuidUtil.getShortUuid());
        this.save(course);
        course.getCategoryIds().forEach(s -> {
            CourseCategory entity = new CourseCategory();
            entity.setCourseId(course.getId());
            entity.setCategoryId(s);
            entity.setId(UuidUtil.getShortUuid());
            courseCategoryService.save(entity);
        });
    }

    @Override
    public void updateContent() {
        testSupplier(()->{
            updateTest();
            return null;
        });
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateTest() {
        Course entity = new Course();
        entity.setId(UuidUtil.getShortUuid());
        entity.setCharge("0");
        entity.setCreatedAt(new Date());
        entity.setEnroll(0);
        entity.setImage("");
        entity.setName("00");
        this.save(entity);
        throw new RuntimeException("999999");
    }

    public void testSupplier(Supplier supplier){
        supplier.get();
    }

}
