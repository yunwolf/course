package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.dto.CourseDto;
import com.yulang.server.pojo.Course;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
public interface CourseService extends IService<Course> {

    void saveCourse(CourseDto course);

    void updateContent();

    void updateTest();
}
