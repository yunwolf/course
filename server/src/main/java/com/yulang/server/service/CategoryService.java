package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.dto.IdLabelChildren;
import com.yulang.server.pojo.Category;

import java.util.List;

/**
 * <p>
 * 分类 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
public interface CategoryService extends IService<Category> {

    List<IdLabelChildren> listAll();
}
