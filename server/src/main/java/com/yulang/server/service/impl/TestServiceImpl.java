package com.yulang.server.service.impl;

import com.yulang.server.mapper.TestMapper;
import com.yulang.server.pojo.Test;
import com.yulang.server.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TestServiceImpl implements TestService {

    @Autowired
    private TestMapper mapper;

    @Override
    public List<Test> list() {
        return mapper.list();
    }
}
