package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.pojo.Chapter;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-23
 */
public interface ChapterService extends IService<Chapter> {

}
