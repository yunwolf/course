package com.yulang.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulang.server.dto.IdLabel;
import com.yulang.server.dto.IdLabelChildren;
import com.yulang.server.mapper.CategoryMapper;
import com.yulang.server.pojo.Category;
import com.yulang.server.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 分类 服务实现类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Override
    public List<IdLabelChildren> listAll() {
        QueryWrapper<Category> query = new QueryWrapper<>();
        query.eq(Category.PARENT,"00000000");
        List<Category> list = this.list(query);
        List<IdLabelChildren> idLabelChildrens = new ArrayList<>();
        list.forEach(item->{
            IdLabelChildren labelChildren = new IdLabelChildren();
            List<IdLabel> labelList = new ArrayList<>();
            QueryWrapper<Category> queryItem = new QueryWrapper<>();
            queryItem.eq(Category.PARENT,item.getId());
            List<Category> items = this.list(queryItem);
            items.forEach(i->{
                IdLabel idLabel = new IdLabel();
                idLabel.setId(i.getId());
                idLabel.setLabel(i.getName());
                labelList.add(idLabel);
            });
            labelChildren.setId(item.getId());
            labelChildren.setLabel(item.getName());
            labelChildren.setChildren(labelList);
            idLabelChildrens.add(labelChildren);
        });
        return idLabelChildrens;
    }
}
