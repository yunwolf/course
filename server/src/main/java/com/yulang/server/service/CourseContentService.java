package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.pojo.CourseContent;

/**
 * <p>
 * 课程内容 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
public interface CourseContentService extends IService<CourseContent> {

}
