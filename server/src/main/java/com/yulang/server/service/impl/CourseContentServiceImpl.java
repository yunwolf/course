package com.yulang.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yulang.server.mapper.CourseContentMapper;
import com.yulang.server.pojo.CourseContent;
import com.yulang.server.service.CourseContentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程内容 服务实现类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Service
public class CourseContentServiceImpl extends ServiceImpl<CourseContentMapper, CourseContent> implements CourseContentService {

}
