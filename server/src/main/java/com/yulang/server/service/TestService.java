package com.yulang.server.service;

import com.yulang.server.pojo.Test;

import java.util.List;

public interface TestService {

    public List<Test> list();

}
