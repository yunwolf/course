package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.pojo.CourseCategory;

/**
 * <p>
 * 课程分类 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
public interface CourseCategoryService extends IService<CourseCategory> {

}
