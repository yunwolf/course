package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.pojo.File;

/**
 * <p>
 * 文件 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
public interface FileService extends IService<File> {

}
