package com.yulang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yulang.server.pojo.Teacher;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
public interface TeacherService extends IService<Teacher> {

}
