package com.yulang.server.exception;

import com.yulang.server.response.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ExceptionHandler{

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public CommonResponse exception(Exception e){
        log.info(e.getMessage());
        return CommonResponse.error(e.getMessage());
    }

}
