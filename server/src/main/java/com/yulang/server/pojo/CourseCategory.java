package com.yulang.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 课程分类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("course_category")
public class CourseCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 课程|course.id
     */
    @TableField(COURSE_ID)
    private String courseId;

    /**
     * 分类|course.id
     */
    @TableField(CATEGORY_ID)
    private String categoryId;


    public static final String ID = "`id`";

    public static final String COURSE_ID = "`course_id`";

    public static final String CATEGORY_ID = "`category_id`";


    public static final String ID_COMMENT = "id";

    public static final String COURSE_ID_COMMENT = "课程|course.id";

    public static final String CATEGORY_ID_COMMENT = "分类|course.id";



    public static CourseCategory of(Object obj) {
        CourseCategory e = new CourseCategory();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<CourseCategory> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
