package com.yulang.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 讲师
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("teacher")
public class Teacher implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 姓名
     */
    @TableField(NAME)
    private String name;

    /**
     * 昵称
     */
    @TableField(NICKNAME)
    private String nickname;

    /**
     * 头像
     */
    @TableField(IMAGE)
    private String image;

    /**
     * 职位
     */
    @TableField(POSITION)
    private String position;

    /**
     * 座右铭
     */
    @TableField(MOTTO)
    private String motto;

    /**
     * 简介
     */
    @TableField(INTRO)
    private String intro;


    public static final String ID = "`id`";

    public static final String NAME = "`name`";

    public static final String NICKNAME = "`nickname`";

    public static final String IMAGE = "`image`";

    public static final String POSITION = "`position`";

    public static final String MOTTO = "`motto`";

    public static final String INTRO = "`intro`";


    public static final String ID_COMMENT = "id";

    public static final String NAME_COMMENT = "姓名";

    public static final String NICKNAME_COMMENT = "昵称";

    public static final String IMAGE_COMMENT = "头像";

    public static final String POSITION_COMMENT = "职位";

    public static final String MOTTO_COMMENT = "座右铭";

    public static final String INTRO_COMMENT = "简介";



    public static Teacher of(Object obj) {
        Teacher e = new Teacher();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<Teacher> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
