package com.yulang.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 分类
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("category")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 父id
     */
    @TableField(PARENT)
    private String parent;

    /**
     * 名称
     */
    @TableField(NAME)
    private String name;

    /**
     * 顺序
     */
    @TableField(SORT)
    private Integer sort;


    public static final String ID = "`id`";

    public static final String PARENT = "`parent`";

    public static final String NAME = "`name`";

    public static final String SORT = "`sort`";


    public static final String ID_COMMENT = "id";

    public static final String PARENT_COMMENT = "父id";

    public static final String NAME_COMMENT = "名称";

    public static final String SORT_COMMENT = "顺序";



    public static Category of(Object obj) {
        Category e = new Category();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<Category> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
