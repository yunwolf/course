package com.yulang.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 文件
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("file")
public class File implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 相对路径
     */
    @TableField(PATH)
    private String path;

    /**
     * 文件名
     */
    @TableField(NAME)
    private String name;

    /**
     * 后缀
     */
    @TableField(SUFFIX)
    private String suffix;

    /**
     * 大小|字节B
     */
    @TableField(SIZE)
    private Integer size;

    /**
     * 用途|枚举[FileUseEnum]：COURSE("C", "讲师"), TEACHER("T", "课程")
     */
    @TableField(USE)
    private String use;

    /**
     * 创建时间
     */
    @TableField(CREATED_AT)
    private Date createdAt;

    /**
     * 修改时间
     */
    @TableField(UPDATED_AT)
    private Date updatedAt;

    /**
     * 已上传分片
     */
    @TableField(SHARD_INDEX)
    private Integer shardIndex;

    /**
     * 分片大小|B
     */
    @TableField(SHARD_SIZE)
    private Integer shardSize;

    /**
     * 分片总数
     */
    @TableField(SHARD_TOTAL)
    private Integer shardTotal;

    /**
     * 文件标识
     */
    @TableField(KEY)
    private String key;

    /**
     * vod|阿里云vod
     */
    @TableField(VOD)
    private String vod;


    public static final String ID = "`id`";

    public static final String PATH = "`path`";

    public static final String NAME = "`name`";

    public static final String SUFFIX = "`suffix`";

    public static final String SIZE = "`size`";

    public static final String USE = "`use`";

    public static final String CREATED_AT = "`created_at`";

    public static final String UPDATED_AT = "`updated_at`";

    public static final String SHARD_INDEX = "`shard_index`";

    public static final String SHARD_SIZE = "`shard_size`";

    public static final String SHARD_TOTAL = "`shard_total`";

    public static final String KEY = "`key`";

    public static final String VOD = "`vod`";


    public static final String ID_COMMENT = "id";

    public static final String PATH_COMMENT = "相对路径";

    public static final String NAME_COMMENT = "文件名";

    public static final String SUFFIX_COMMENT = "后缀";

    public static final String SIZE_COMMENT = "大小|字节B";

    public static final String USE_COMMENT = "用途|枚举[FileUseEnum]：COURSE('C', '讲师'), TEACHER('T', '课程')";

    public static final String CREATED_AT_COMMENT = "创建时间";

    public static final String UPDATED_AT_COMMENT = "修改时间";

    public static final String SHARD_INDEX_COMMENT = "已上传分片";

    public static final String SHARD_SIZE_COMMENT = "分片大小|B";

    public static final String SHARD_TOTAL_COMMENT = "分片总数";

    public static final String KEY_COMMENT = "文件标识";

    public static final String VOD_COMMENT = "vod|阿里云vod";



    public static File of(Object obj) {
        File e = new File();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<File> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
