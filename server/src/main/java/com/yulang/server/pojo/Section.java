package com.yulang.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 小节
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("section")
public class Section implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 标题
     */
    @TableField(TITLE)
    private String title;

    /**
     * 课程|course.id
     */
    @TableField(COURSE_ID)
    private String courseId;

    /**
     * 大章|chapter.id
     */
    @TableField(CHAPTER_ID)
    private String chapterId;

    /**
     * 视频
     */
    @TableField(VIDEO)
    private String video;

    /**
     * 时长|单位秒
     */
    @TableField(TIME)
    private Integer time;

    /**
     * 收费|C 收费；F 免费
     */
    @TableField(CHARGE)
    private String charge;

    /**
     * 顺序
     */
    @TableField(SORT)
    private Integer sort;

    /**
     * 创建时间
     */
    @TableField(CREATED_AT)
    private Date createdAt;

    /**
     * 修改时间
     */
    @TableField(UPDATED_AT)
    private Date updatedAt;

    /**
     * vod|阿里云vod
     */
    @TableField(VOD)
    private String vod;


    public static final String ID = "`id`";

    public static final String TITLE = "`title`";

    public static final String COURSE_ID = "`course_id`";

    public static final String CHAPTER_ID = "`chapter_id`";

    public static final String VIDEO = "`video`";

    public static final String TIME = "`time`";

    public static final String CHARGE = "`charge`";

    public static final String SORT = "`sort`";

    public static final String CREATED_AT = "`created_at`";

    public static final String UPDATED_AT = "`updated_at`";

    public static final String VOD = "`vod`";


    public static final String ID_COMMENT = "id";

    public static final String TITLE_COMMENT = "标题";

    public static final String COURSE_ID_COMMENT = "课程|course.id";

    public static final String CHAPTER_ID_COMMENT = "大章|chapter.id";

    public static final String VIDEO_COMMENT = "视频";

    public static final String TIME_COMMENT = "时长|单位秒";

    public static final String CHARGE_COMMENT = "收费|C 收费；F 免费";

    public static final String SORT_COMMENT = "顺序";

    public static final String CREATED_AT_COMMENT = "创建时间";

    public static final String UPDATED_AT_COMMENT = "修改时间";

    public static final String VOD_COMMENT = "vod|阿里云vod";



    public static Section of(Object obj) {
        Section e = new Section();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<Section> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
