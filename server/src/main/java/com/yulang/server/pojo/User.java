package com.yulang.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author Administrator
 * @since 2020-05-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 登陆名
     */
    @TableField(LOGIN_NAME)
    private String loginName;

    /**
     * 昵称
     */
    @TableField(NAME)
    private String name;

    /**
     * 密码
     */
    @TableField(PASSWORD)
    private String password;


    public static final String ID = "`id`";

    public static final String LOGIN_NAME = "`login_name`";

    public static final String NAME = "`name`";

    public static final String PASSWORD = "`password`";


    public static final String ID_COMMENT = "id";

    public static final String LOGIN_NAME_COMMENT = "登陆名";

    public static final String NAME_COMMENT = "昵称";

    public static final String PASSWORD_COMMENT = "密码";



    public static User of(Object obj) {
        User e = new User();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<User> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
