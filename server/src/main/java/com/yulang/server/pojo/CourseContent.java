package com.yulang.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 课程内容
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("course_content")
public class CourseContent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 课程内容
     */
    @TableField(CONTENT)
    private String content;


    public static final String ID = "`id`";

    public static final String CONTENT = "`content`";


    public static final String ID_COMMENT = "课程id";

    public static final String CONTENT_COMMENT = "课程内容";



    public static CourseContent of(Object obj) {
        CourseContent e = new CourseContent();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<CourseContent> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
