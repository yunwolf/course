package com.yulang.server.pojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * <p>
 * 课程
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("course")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = ID, type = IdType.INPUT)
    private String id;

    /**
     * 名称
     */
    @TableField(NAME)
    private String name;

    /**
     * 概述
     */
    @TableField(SUMMARY)
    private String summary;

    /**
     * 时长|单位秒
     */
    @TableField(TIME)
    private Integer time;

    /**
     * 价格（元）
     */
    @TableField(PRICE)
    private BigDecimal price;

    /**
     * 封面
     */
    @TableField(IMAGE)
    private String image;

    /**
     * 级别|枚举[CourseLevelEnum]：ONE("1", "初级"),TWO("2", "中级"),THREE("3", "高级")
     */
    @TableField(LEVEL)
    private String level;

    /**
     * 收费|枚举[CourseChargeEnum]：CHARGE("C", "收费"),FREE("F", "免费")
     */
    @TableField(CHARGE)
    private String charge;

    /**
     * 状态|枚举[CourseStatusEnum]：PUBLISH("P", "发布"),DRAFT("D", "草稿")
     */
    @TableField(STATUS)
    private String status;

    /**
     * 报名数
     */
    @TableField(ENROLL)
    private Integer enroll;

    /**
     * 顺序
     */
    @TableField(SORT)
    private Integer sort;

    /**
     * 创建时间
     */
    @TableField(CREATED_AT)
    private Date createdAt;

    /**
     * 修改时间
     */
    @TableField(UPDATED_AT)
    private Date updatedAt;

    /**
     * 讲师|teacher.id
     */
    @TableField(TEACHER_ID)
    private String teacherId;


    public static final String ID = "`id`";

    public static final String NAME = "`name`";

    public static final String SUMMARY = "`summary`";

    public static final String TIME = "`time`";

    public static final String PRICE = "`price`";

    public static final String IMAGE = "`image`";

    public static final String LEVEL = "`level`";

    public static final String CHARGE = "`charge`";

    public static final String STATUS = "`status`";

    public static final String ENROLL = "`enroll`";

    public static final String SORT = "`sort`";

    public static final String CREATED_AT = "`created_at`";

    public static final String UPDATED_AT = "`updated_at`";

    public static final String TEACHER_ID = "`teacher_id`";


    public static final String ID_COMMENT = "id";

    public static final String NAME_COMMENT = "名称";

    public static final String SUMMARY_COMMENT = "概述";

    public static final String TIME_COMMENT = "时长|单位秒";

    public static final String PRICE_COMMENT = "价格（元）";

    public static final String IMAGE_COMMENT = "封面";

    public static final String LEVEL_COMMENT = "级别|枚举[CourseLevelEnum]：ONE('1', '初级'),TWO('2', '中级'),THREE('3', '高级')";

    public static final String CHARGE_COMMENT = "收费|枚举[CourseChargeEnum]：CHARGE('C', '收费'),FREE('F', '免费')";

    public static final String STATUS_COMMENT = "状态|枚举[CourseStatusEnum]：PUBLISH('P', '发布'),DRAFT('D', '草稿')";

    public static final String ENROLL_COMMENT = "报名数";

    public static final String SORT_COMMENT = "顺序";

    public static final String CREATED_AT_COMMENT = "创建时间";

    public static final String UPDATED_AT_COMMENT = "修改时间";

    public static final String TEACHER_ID_COMMENT = "讲师|teacher.id";



    public static Course of(Object obj) {
        Course e = new Course();
        BeanUtils.copyProperties(obj, e);
        return e;
    }

    public static QueryWrapper<Course> queryWrapper(Object obj) {
        return new QueryWrapper<>(of(obj));
    }

}
