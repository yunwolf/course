package com.yulang.server.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class CommonResponse<T> {

    private Integer status;

    private String msg;

    private T data;

    public static CommonResponse ok(){
        return new CommonResponse(HttpStatus.OK.value(),"调用成功",null);
    }

    public static CommonResponse ok(Object data){
        return new CommonResponse(HttpStatus.OK.value(),"调用成功",data);
    }

    public static CommonResponse error(String message) {
        return new CommonResponse(HttpStatus.BAD_REQUEST.value(),message,null);
    }
}
