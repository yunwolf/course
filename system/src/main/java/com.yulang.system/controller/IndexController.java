package com.yulang.system.controller;

import com.yulang.server.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index")
public class IndexController {

    String str = new String("hello");

    @Autowired
    private TestService service;

    @RequestMapping("/hello")
    public Object index(){
        return service.list();
    }

    public static void main(String[] args) {
        String s1 = "hello";
        String s2 = "hell";
        String s3 = "o";
        String s4 = s2 + s3;
        System.out.println(s1==s4);
    }

    public void exchange(String str ){
        str = "test ok";
    }

}
