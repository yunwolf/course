package com.yulang.business.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yulang.server.dto.Pageable;
import com.yulang.server.pojo.Section;
import com.yulang.server.response.CommonResponse;
import com.yulang.server.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 小节 前端控制器
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@RestController
@RequestMapping("/section")
public class SectionController {

    @Autowired
    private SectionService sectionService;

    @PostMapping("/list")
    public CommonResponse list(@RequestBody Pageable pageable){
        Page res = sectionService.page(pageable.toPage());
        return CommonResponse.ok(res);
    }

    @DeleteMapping("/delete/{id}")
    public CommonResponse del(@PathVariable String id){
        sectionService.removeById(id);
        return CommonResponse.ok();
    }

    @PostMapping("/add")
    public CommonResponse del(@RequestBody Section section){
        sectionService.saveSection(section);
        return CommonResponse.ok();
    }

}

