package com.yulang.business.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yulang.server.dto.CourseDto;
import com.yulang.server.dto.Pageable;
import com.yulang.server.pojo.Course;
import com.yulang.server.pojo.CourseContent;
import com.yulang.server.response.CommonResponse;
import com.yulang.server.service.CourseContentService;
import com.yulang.server.service.CourseService;
import com.yulang.server.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private CourseService service;

    @Autowired
    private CourseContentService courseContentService;

    @PostMapping("/list")
    public CommonResponse list(@RequestBody Pageable pageable){
        Page page = service.page(pageable.toPage());
        return CommonResponse.ok(page);
    }


    @DeleteMapping("/delete/{id}")
    public CommonResponse delete(@PathVariable String id){
        service.removeById(id);
        return CommonResponse.ok();
    }

    @PostMapping("/add")
    public CommonResponse delete(@RequestBody CourseDto course){
        course.setId(UuidUtil.getShortUuid());
        service.saveCourse(course);
        return CommonResponse.ok();
    }

    @PostMapping("/add-content")
    public CommonResponse addContent(@RequestBody CourseContent courseContent){
        courseContentService.save(courseContent);
        return CommonResponse.ok();
    }

    @PostMapping("/update")
    public CommonResponse update(){
        service.updateContent();
        return CommonResponse.ok();
    }


}
