package com.yulang.business.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yulang.server.dto.CategoryDto;
import com.yulang.server.dto.IdLabelChildren;
import com.yulang.server.pojo.Category;
import com.yulang.server.response.CommonResponse;
import com.yulang.server.service.CategoryService;
import com.yulang.server.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 分类 前端控制器
 * </p>
 *
 * @author Administrator
 * @since 2020-04-25
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/list")
    public CommonResponse list(){
        QueryWrapper<Category> query = new QueryWrapper<>();
        query.eq(Category.PARENT,"00000000");
        List<Category> list = categoryService.list(query);
        return CommonResponse.ok(list);
    }

    @PostMapping("/add")
    public CommonResponse add(@RequestBody Category category){
        category.setId(UuidUtil.getShortUuid());
        categoryService.save(category);
        return CommonResponse.ok();
    }

    @DeleteMapping("/{id}")
    public CommonResponse del(@PathVariable String id){
        categoryService.removeById(id);
        return CommonResponse.ok();
    }


    @GetMapping("/list-item/{id}")
    public CommonResponse listItem(@PathVariable String id){
        QueryWrapper<Category> query = new QueryWrapper<>();
        query.eq(Category.PARENT,id);
        List<Category> list = categoryService.list(query);
        return CommonResponse.ok(list);
    }

    @GetMapping("/list-all")
    public CommonResponse category(){
       List<IdLabelChildren> idLabelChildren = categoryService.listAll();
       return CommonResponse.ok(idLabelChildren);
    }



}

