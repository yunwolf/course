package com.yulang.business.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yulang.server.dto.Pageable;
import com.yulang.server.pojo.Chapter;
import com.yulang.server.response.CommonResponse;
import com.yulang.server.service.ChapterService;
import com.yulang.server.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/chapter")
public class ChapterController {

    @Autowired
    private ChapterService service;

    @PostMapping
    public CommonResponse list(@RequestBody Pageable pageable){
        Page page = service.page(pageable.toPage());
        return CommonResponse.ok(page);
    }

    @PostMapping("/add")
    public CommonResponse add(@RequestBody Chapter chapter){
        chapter.setId(UuidUtil.getShortUuid());
        service.save(chapter);
        return CommonResponse.ok();
    }

}
