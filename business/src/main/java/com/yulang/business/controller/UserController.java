package com.yulang.business.controller;


import com.yulang.server.response.CommonResponse;
import com.yulang.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author Administrator
 * @since 2020-05-18
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/list")
    public CommonResponse list(){
       return CommonResponse.ok(userService.list()) ;
    }

}

