package com.yulang.business.controller;


import com.yulang.server.dto.Pageable;
import com.yulang.server.response.CommonResponse;
import com.yulang.server.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @PostMapping("/list")
    public CommonResponse list(@RequestBody Pageable pageable){
        return CommonResponse.ok(teacherService.page(pageable.toPage()));
    }

}

