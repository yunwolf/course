package com.yulang;

import com.yulang.server.service.CourseService;
import net.hasor.spring.boot.EnableHasor;
import net.hasor.spring.boot.EnableHasorWeb;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableEurekaClient
@EnableHasor
@EnableHasorWeb
public class BusinessApp {

    public static void main(String[] args) {
        SpringApplication.run(BusinessApp.class,args);
    }

}
