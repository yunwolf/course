package com.yulang.file.controller;


import com.yulang.server.dto.Pageable;
import com.yulang.server.response.CommonResponse;
import com.yulang.server.service.FileService;
import com.yulang.server.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * <p>
 * 文件 前端控制器
 * </p>
 *
 * @author Administrator
 * @since 2020-04-26
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @PostMapping("/list")
    public CommonResponse list(@RequestBody Pageable pageable){
        return CommonResponse.ok(fileService.page(pageable.toPage()));
    }


    @RequestMapping("/upload")
    public CommonResponse list(MultipartFile file) throws IOException {

        String fileName = file.getOriginalFilename();
        String key = UuidUtil.getShortUuid();
        String fullPath = "E://file/teacher/"+key+"_"+fileName;
        File dest = new File(fullPath);
        file.transferTo(dest);
        return CommonResponse.ok(dest.getAbsoluteFile());
    }

}

