import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import axios from 'axios'

Vue.config.productionTip = false;
Vue.prototype.$ajax = axios
Vue.use(ElementUI)

new Vue({
  router,
  render: h => h(App)
}).$mount("#app")
