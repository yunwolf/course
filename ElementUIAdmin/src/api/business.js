import {buildApiRequest as A,getApiUrl as G} from './init'

export default {
    chapter: (p,c) => A(p,'business/chapter','post',c),
    chapterAdd: (p,c) => A(p,'business/chapter/add','post',c),
    courseList:(p,c) => A(p,'business/course/list','post',c),
    courseAdd:(p,c) => A(p,'business/course/add','post',c),
    courseDelete:(p,c) => A(p,`business/course/delete/${p}`,'delete',c),
    contentAdd: (p,c) => A(p,'business/course/add-content','post',c),
    sectionList: (p,c) => A(p,'business/section/list','post',c),
    sectionDelete: (p,c) => A(p,`business/section/delete/${p}`,'delete',c),
    sectionAdd: (p,c) => A(p,'business/section/add','post',c),
    categoryList: (p,c) => A(p,'business/category/list','post',c),
    categoryDelete: (p,c) => A(p,`business/category/delete/${p}`,'delete',c),
    categoryAdd: (p,c) => A(p,'business/category/add','post',c),
    categoryItem: (p) => A(p,`business/category/list-item/${p}`,'get'),
    categoryAll: (p) => A(p,`business/category/list-all`,'get'),
    teacherList: (p,c) => A(p,'business/teacher/list','post',c),

    userList: (p,c) => A(p,'business/user/list','get',c),

}


