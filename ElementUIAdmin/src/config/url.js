
const devUrl = 'http://127.0.0.1:9000';
const proUrl = 'http://apiUrl.com';


export default {
  apiUrl : __DEV__ ? devUrl : proUrl,
  apiPrefix : '',
  gitHub : 'https://github.com/xusenlin/ElementUIAdmin'
}

